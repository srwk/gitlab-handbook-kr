---
layout: markdown_page
title: "깃랩 핸드북 사용법"
---

## On this page
{:.no_toc}

- TOC
{:toc}

* 이 글의 원문은 https://about.gitlab.com/handbook/handbook-usage/ 입니다.

## 이익 (Advantages)

GitLab에서 우리의 핸드북은 광범위하고, 적절한 관련성을 유지하는 것이 모든 사람들의 일에 중요한 부분입니다.
이 핸드북에 기술된 우리의 절차가 필요한 이유는 다음과 같습니다:

1. 읽는 것은 듣는 것보다 훨씬 빠릅니다.
2. 읽는 것은 비동기적이라서 누군가를 방해하거나 그들이 시간이 생길때 까지 기다릴 필요가 없습니다.
3. 우리가 무엇을 하고 어떻게 운영하는지 사람들이 볼 수 있다면 고용도 더 쉽습니다.
4. 사람들이 가입하기 전에 그들이 무엇을 얻을지 안다면 더 잘 붙잡아 둘 수 있습니다.
5. 관련된 모든 정보를 찾아 볼 수 있다면, 온보딩이 쉬워집니다.(역주: 온보딩(On-Boarding)은 원래 배에 오른다는 뜻입니다. 이 경우 새로운 직원이 회사에서 일하기 위해 필요한 지식, 기술 등을 오리엔테이션이나 트레이닝등을 통해 배우는 과정을 의미합니다.)
6. 회사의 다른 사람들이 어떻게 일하는지 안다면 팀웍이 쉬워집니다.
7. 현재 프로세스가 무엇인지 읽을 수 있다면, 변화에 대한 토론이 쉬워집니다.
8. 차이만 얘기해도 된다면, 변화에 대한 의사소통이 쉬워집니다.
9. 머지 리퀘스트를 통해 변경사항을 제안하는 것으로 모든 사람들이 기여할 수 있습니다.

## 흐름 (Flow)

1. (프로세스의) 문제가 이슈나 챗을 통해 자주 거론됩니다.
2. 머지 리퀘스트를 통해 핸드북에 제안이 만들어집니다.
3. 머지 이후, MR 이나 커밋의 diff 링크와 함께 변경사항이 공표됩니다. 주요사항들은 팀 어젠더에 추가됩니다. 중급 사항들은 챗 채널에 공표됩니다. 관련 이슈가 있었다면 변경사항에 대한 링크를 추가하고 이슈를 닫습니다.

## 왜 핸드북 우선주의 인가? (Why handbook first)

핸드북에 문서를 작성하는 것은 초기에 더 많은 시간을 필요로 합니다.
당신은 어디에 변경사항을 만들지 생각해 보고, 그것을 기존의 내용과 결합하고, 아마도 그것을 핸드북에 추가하거나 일관성 있게 핸드북 내용을 재조정해야 합니다.
하지만 문서는 긴 기간에 걸쳐 시간을 절약해 주고 우리의 조직을 확장하고 적응시키는데 필수적입니다.
이것은 당신의 소프트웨어를 위한 테스트를 작성하는 것과 다르지 않습니다.
오직 (제안된) 변경에 대해 핸드북을 변경하여 의사소통하세요. 프리젠테이션이나, 이메일, 챗 메시지 또는 다른 매체를 통해서 변경의 요점을 전달하지 마세요.
그런 방법들은 제안하는 사람에게는 편할지 몰라도 듣는 사람들이 다른 프로세스들에 대한 문맥과 의미를 이해하는 것을 어렵게 만듭니다.
핸드북 우선주의는 중복이 없는 지 확인하는 것, 핸드북이 항상 업데이트 되는 것, 다른 사람들이 기여할 수 있도록 하는 것을 필요로 합니다.

## 가이드라인 (Guidelines)

부디 이 가이드라인을 따르시고 다른 사람들에게도 상기시켜 주세요.

1. 이 핸드북의 대부분의 가이드 라인은 도움을 주기 위한 것이며 따로 명시되어 있지 않는 한 절대적인 규칙이 되기 보다는 도움을 더 주기 위한 것입니다. 전체 핸드북에 대해 파악하지 못했다는 이유로 무언가를 하기를 두려워하지 마세요. 어차피 핸드북 전체를 아는 사람은 없습니다. 사람들에게 가이드라인에 대해 상기시킬때는 친절해지세요. 예를 들어 "이게 별 문제는 아닌데, 다만 다음엔 핸드북의 가이드라인을 따르는 걸 고려해주세요?" 라고 말하세요.
2. 만약 당신의 질문에 누군가 핸드북의 링크로 답한다면, 그들은 더 자세한 정보를 제공하여 돕고 싶어하는 것입니다. 그리고 그들은 그 답변이 문서화되어 있다는 것을 자랑스럽게 여길 것입니다. 이것은 당신이 전체 핸드북을 다 읽어야 한다는 것을 의미하지 않습니다. 전체 핸드북을 다 아는 사람은 없습니다.
3. 만약 도움을 받기 위해 팀 멤버와 논의할 필요가 있었다면, 아마도 커뮤니티의 다른 사람들도 대부분 그 문제에 대해 잘 모를 수 있다는 점을 생각하세요. 그리고 전체 커뮤니티에 정보가 전달될 수 있도록 답을 **문서화** 하는 것을 잊지 마세요. 질문에 대한 답변을 얻은 후에는 그것이 어디에 문서화 되어야 하고 누가 할 것인지에 대해서 논의하세요. 당신은 "누가 이것을 문서화 할거지?" 라고 물음으로써 다른 사람들에게 이것을 상기시킬 수 있습니다.
4. 어떤 것에 대해서 챗팅으로 논의할 땐 질문에 관련된 문서나 답변이 나와있는 페이지 같은 관련된 URL을 **링크** 하도록 항상 노력하세요. "링크를 부탁해도 될까요?" 라고 말함으로써 다른 사람들에게 이것을 상기시킬 수 있습니다.
5. 가이드라인이나 프로세스를 바꾸려면, 머지 리퀘스트 형식으로 **편집을 제안하세요**. 그것이 머지된 후에, 당신은 해당되는 팀 통화에서 그것에 대해 말할 수 있습니다. "핸드북에 머지리퀘스트를 보내주실 수 있나요?" 라고 말함으로써 다른 사람들에게 이것을 상기시킬 수 있습니다.
6. **머지된 diff** (변경 이전과 이후의 차이를 보여주는 커밋)를 링크함으로써 프로세스의 변경에 대해 얘기하세요. 만약 당신이 논의나 피드백을 목적으로 변경에 대해 얘기하고 있다면, **머지되지 않은 diff** 를 링크하는 것도 좋습니다. 프로세스를 먼저 바꾸고 나서 다음 할일로 문서를 보는 것은 안됩니다.  나중에 문서화하려고 하면 필연적으로 변경을 알리는 중복 작업이 필요하고 또 오래된(outdated) 문서를 만들게 됩니다. "핸드북을 먼저 업데이트해주시겠어요?" 라고 말함으로써 사람들이게 이것을 상기시킬 수 있습니다.
7. 기억하세요. 핸드북은 우리가 하기를 원하는 것이나, 공식적으로 해야하는 것이나, 우리가 전에 했던 것에 대한 것이 아닙니다. **그것은 지금 우리가 하는 것** 에 대한 것입니다. 따라서 프로세스를 바꾸고 싶다면, 그에 따라 핸드북도 바뀌어야 합니다. 프로세스 변경을 제안하고 싶다면 핸드북을 변경하는 머지 리퀘스트를 만드세요. 핸드북 변경을 제안하기 위해 다른 채널(이메일, 구글독 등)을 사용하지 마세요.
8. 다른 모든 것들과 마찬가지로, 우리의 프로세스들도 항상 유동적입니다. 모든 것이 언제나 초안(draft)에 있고, 초기 버전도 핸드북에 있어야 합니다. 핸드북의 변경을 제안할 땐 가능하면 이슈는 건너뛰고 바로 머지 리퀘스트를 하세요. 많은 경우, 제안된 변경사항을 바로 볼 수 있는 머지 리퀘스트가 협업하기 더 쉽습니다.
9. 변경을 제안하기 할때 머지 리퀘스트가 이슈 설명보다 더 좋습니다. 머지 리퀘스트는 사람들이 변경사항의 문맥을 볼 수 있도록 해줍니다.
10. 어떤 것이 한 그룹의 유저들에게 제한된 테스트일 경우, 그것을 핸드북에 추가하고, 그에 대한 노트를 적으세요. 테스트가 끝나고 모든 경우 새로운 프로세스를 사용해야 하게 되면 그 노트를 지우세요.
11. 어떤 것에 대해 의사소통 할때는 내용 전체를 이메일/채팅/기타등등에 포함하는 대신 항상 관련된 (그리고 업데이트된) 핸드북의 내용을 링크하세요. "관련된 핸드북 페이지를 링크해주실 수 있나요?" 라고 말함으로써 사람들에게 이것을 상기시킬 수 있습니다.
12. 핸드북의 내용을 다른 페이지로 복사할땐, 부디 원래 내용을 지우고 새로운 페이지에 대한 링크로 교체해 주세요. 스스로를 반복하지 않도록(DRY: Don't Repeat Yourself), 정보 아키텍쳐에 대해서 생각해 주세요. 컨텐츠 중복은 잘못된 장소에서 업데이트를 만듭니다. [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) 를 지켜주세요.
13. 관련된 항목들(핸드북의 다른 곳, 문서, 이슈)이 있는 경우는 항상 아이템들을 교차링크(cross-link)해야 합니다.
14. 핸드북은 기능과 결과로 구조화하여 핸드북 내의 모든 아이템들이 항상 최신의 상태를 유지하도록 명확한 소유자와 위치를 가져야 합니다. 부디 사람들을 다른 섹션으로 안내하는 크로스링크를 자유롭게 작성해 주세요. FAQ, 링크목록, 용어집, 강좌, 비디오, 테스트, 하우투 같은 포맷을 기반으로한 비구조화된 컨텐츠는 피해주세요. 그런 것 들은 최신 상태를 쥬지하기 어렵고 기능과 결과 별 조직과 잘 호환되지 않습니다. 대신 답변, 링크, 정의, 코스, 비디오, 테스트를 가장 관련된 장소에 넣으세요.(역주: 이부분 번역이 좀 안맞는 것 같습니다.) 사람들이 컨텐츠를 쉽게 검색할 수 있도록 설명 헤더를 사용하세요. 같은 페이지에 서로 다른 페이지를 섞어서 다른 종류의 포맷을 좋아하는 사람들이 좋아하는 포맷을 사용할 수 있게 해주세요. 서로다른 타임의 컨텐츠를 내장할땐 어떻게 보일까가 아니라, 기능/결과별 조직에 대해 걱정하세요.
15. 핸드북은 그 자체가 프로세스입니다. '프로세스', '정책', '베스트 프랙티스', '표준 운영 절차' 와 같은 이름의 섹션은 서로 결합되어 기술되어야 하는 프로세스에 대한 산문체 설명과 같은 프로세스에 대한 산술식 설명(a numbered list  description) 사이의 중복과 같은 식간한 문제를 나타냅니다.
16. 헤더를 자유롭게 사용하세요. 만약 페이지가 스크린보다 길어지면, [이 MR의 줄 6에서 10을](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/7141/diffs#f054d0f855ebef2a11559c362a356a2f9e010b99_6_6) 복사해서 자동으로 만들어지는 목차(Toc; Table of Contents)를 넣으세요. ). 데스크탑에서는 ToC가 사이드바에 나타나지만 모바일에선 아니기 때문에 헤더가 두개 이상이면 넣는 것이 좋습니다. 헤더는 일반적으로 대소문자화 되어야 합니다. ([ALL CAPS](https://en.wikipedia.org/wiki/All_caps)나 [TitleCase](http://www.grammar-monster.com/glossary/title_case.htm) 를 사용하지 마세요). 헤더 다음에 빈줄을 넣으세요. 이것이 [표준에 필요한 것은 아니지만](http://spec.commonmark.org/0.27/#example-46) 우리 규칙입니다.
17. 깃랩 내부나 외부의 사람이 좋은 제안을 했다면 핸드북에 추가할 수 있도록 초대하세요. 관련된 페이지나 섹션의 url 을 보내고 그들이 할 수 없다면 도와주세요. 그들이 그것을 만들고 보냄으로써 변경과 검토에 그들의 지식이 반영될 것입니다.
18. 더 넓은 커뮤니티로부터의 코드 기여에도 적용되는 모든 문서들은 핸드북이 아니라 GitLab CE project 내에 있어야 합니다. (예를 들어 [CONTRIBUTING](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md) 이나 [code review guidelines](https://docs.gitlab.com/ee/development/code_review.html). 핸드북은 팀 멤버만을 위한 것입니다.
19. [git을 이용하여 핸드북을을 편집하는 방법](/handbook/git-page-update)을 배우세요. 그리고 기여하기 전에 부디 [작성 스타일 가이드라인](https://about.gitlab.com/handbook/communication/#writing-style-guidelines)을 읽어주세요.
20. 머지 리퀘스트를 제출할땐, 그것이 빠르게 머지될 수 있는지 먼저 확인해야합니다. 하나의 혹은 작은 변경들을 만들면, 당신의 브랜치가 마스터 브랜치로 부터 멀리 떨어지고 머지 충돌이 생기는 것을 막을 수 있습니다. 당신의 업데이트를 만드는 것과 머지하는 것을 하루에 할 수 있도록 노력하세요. 머지 리퀘스트에서 사람들을 언급(mention)하거나 슬랙을 통해 접근하세요. 만약 현재의 것 위에 큰 개선점에 대한 제안을 받는 다면, 그것을 따로 진행하는 것을 고려해보세요. 아슈를 만들고 현재 MR이 머지되고 나면 새 머지리퀘스트를 만드세요.
21. 머지가 너무 빨리 될 경우 회사에 부정적 영향을 미칠 수 있다면 머지 리쿠퀘스트에 "WIP" (Work in Progress) 라고 마크하세요. 어플리케이션 코드엔 이런 경우가 있지만 핸드북 MR엔 WIP를 쓸일이 거의 없습니다.
22. 컨텐츠를 옮겨야 한다면, 그것을 옮기는 머지 리퀘스트를 만들면 끝입니다. 컨텐츠를 정리하거나 요약하거나 확장하고 싶다면 옮기기 MR이 머지된 다음에 합니다. 그래야 리뷰가 쉬워집니다.
23. 이 핸드북의 내용은 (미래의) 깃랩 팀 멤버만을 고려한 것입니다. 깃랩 유져을 위한 내용은 [GitLab 문서](http://doc.gitlab.com/), [GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit), [CONTRIBUTING file](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md), [PROCESS file](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md) 에 있어야 합니다.
24. 핸드북 페이지를 짧고 간결하게 유지하세요. 부풀림은 제거하고, 가능한 짧은 말로 포인트에 도달하세요. 신입 직원들의 언급에 따르면 가장 큰 도전은 온보딩 동안 방대한 양의 정보를 처리하는 것이라는 점을 기억하세요.
25. [.gitkeep 파일들은](http://www.ryanwright.me/cookbook/git/gitkeep) 핸드북에 필요없습니다. 이 것들은 파일을 에디터에서 빠르게 열어보기 어렵게 만듭니다. 그것들을 추가하지 말고, 발견하면 지워버리세요.
26. 오타 정정보다 큰 일이라면 온라인 에디터보다 터미널을 사용하는게 좋습니다. Anything more than a spelling correction is better done in the terminal than with the online editor. 핸드북을 업데이트하는 것을 꺼리는 사람들은 터미널이나 로컬 에디터, 로컬 프리뷰를 사용하지 않습니다. . [웹사이트를 로컬에서 편집하기](https://about.gitlab.com/handbook/git-page-update/) 안내를 읽어주세요.
27. 팀 멤버가 현지 통화로 환전해야 하는 금액에 대해 언급할땐 (예: 혜택, 지출, 보너스), 그 금액에 우리의 [Exchange Rates](/handbook/people-operations/global-compensation/#exchange-rates) 색션 (예: [500 USD](/handbook/people-operations/global-compensation/#exchange-rates))을 링크하세요.

## 관리 (Management)

핸드북을 최신상태로 유지하는 것은 각 부서와 팀 맴버들의 책임입니다.
People Operations는 각 부서의 대표들과 제휴하여 매주 리뷰를 통해 핸드북의 내용이 정확하고 [Guidelines](/handbook/handbook-usage/#guidelines) 의 포맷을 따르는지 검토합니다.
머지리퀘스트 제출에 대해 궁금한 점이나, 핸드북에 도움이 필요하면 깃랩에서 `@gl-handbookupdate` 를 핑(ping) 하세요.

핸드북의 모든 변경은 이 리뷰의 부분으로써 `#handbook` 슬랙 채널에서 공유됩니다.
People Ops 는 `#questions` 채널에 올라오는 질문들을 문서화하고, 팀콜의 모든 공지가 적절한 링크를 가지게 합니다.

## 프리젠테이션을 만드는 대신 핸드북을 화면공유(ScreenShare)

프리젠테이션은 [functional group updates](/handbook/people-operations/functional-group-updates/)나 board presentations 처럼 짧은 생명을 가진 컨텐츠엔 좋습니다. [leadership training](/handbook/leadership/#training) 처럼 [오래 가는(Everygreen) 컨텐츠 content](https://www.thebalance.com/what-is-evergreen-content-definition-dos-and-don-ts-2316028) 는 핸드북에 기초해야 합니다. Everygreen 컨텐츠는 부디 프리젠테이션을 만드는 대신 핸드북을 스크린 쉐어 해주세요. 왜냐하면:

1. 프리젠테이션을 만드는 데 필요한 노력을 아껴줍니다.
2. 사람들이 그 핸드북 섹션을 나중에 쉽게 찾을 수 있습니다.
3. 프리젠테이션 중에 핸드북을 체크하고 개선할 수 있습니다.
4. 컨텐츠에 누구나 기여할 수 있게 됩니다.
5. 컨텐츠가 우리의 다른 프로세스들과 통합됩니다.
6. [핸드북을 사용하면 좋은점](https://about.gitlab.com/handbook/handbook-usage/)도 참고하세요.

## 핸드북을 가치있게 만드세요 (Make it worthwhile)

어떤 회사가 우리가 어떻게 핸드북으로 일을 관리하는지 물어봤습니다. 왜냐하면 그 회사에서는 핸드북이 잘 작동하지 않았기 때문입니다.
"There are many occasions where something is documented in the knowledge base, but people don't know about it because they never bothered to read or search. Some people have a strong aversion against what they perceive as a 'wall of text'."


사람들이 핸드북을 보는데 시간을 잘 사용할 수 있도록, 다음의 것들이 필요합니다:

1. 짧고 간결한 핸드북 페이지
2. 현재 시제와 간단한 단어를 사용
3. 정보 아키텍쳐가 명확하도록 기능별로 정리.
4. 관련 정보를 쉽게 찾을 수 있도록 크로스링크를 자유롭게 사용.
5. 좋은 검색 기능(우리는 Algolia를 사용합니다)
6. 구글 검색을 사용할 수 있도록 공개
7. 깨끗한 url과 단락에 대한 딥링크 허용
8. 자동 목차 생성기 사용
9. 키 메시지를 주는 많은 헤더들
10. 키워드를 강조(bold)
11. 질문에 대해 대답 대신 핸드북 링크를 줌.
12. 온보딩 동안 사람들의 지식을 테스트
13. 중복을 피하고 링크로 대체
14. 진짜 예제
15. 사무적으로 말하지 말고 친구에게 얘기하는 것 처럼 기술
16. numbered lists, unordered lists와 테이블들을 많이 사용.
17. 컨텐츠를 시청할 수 있도록 비디오를 임베드
18. 그림, gifs, 카툰, graphics 를 넣어서 흥미롭고 기억하기 쉽게 만듬.
19. 핸드북에 없는 것에 대한 질문을 받으면 그것을 핸드북에 추가하고 그 diff 를 제공
